var frase = prompt("insira somente texto");
var chave = parseInt(prompt("insira chave numérica"));
function cifrando(codigo, chave){
  var texto = "";
  codigo =  codigo.split("");
  for (var i = 0; i < codigo.length; i++){
    if (codigo[i] === codigo[i].toUpperCase()){
    texto = texto + String.fromCharCode(
      (codigo[i].charCodeAt() - 65 + chave) % 26 + 65);
  } else {
    texto = texto + String.fromCharCode(
      (codigo[i].charCodeAt() - 97 + chave) % 26 + 97);
    }

  }
  return texto;
}
var mensagemCifrada = cifrando(frase, chave);
function decifrando(codigo, chave){
  var texto = "";
  for (var i = 0; i < codigo.length; i++){
    if (codigo[i] === codigo[i].toUpperCase()) {
    texto = texto + String.fromCharCode(
      (codigo[i].charCodeAt() + 65 + (26 - chave)) % 26 + 65);
  } else {
    texto = texto + String.fromCharCode(
      (codigo[i].charCodeAt() + 97 - (26 - chave)) % 26 + 97);
    }
  }
  return texto;
}
decifrando(mensagemCifrada, chave);
var mensagemDecifrada = decifrando(mensagemCifrada, chave);
document.getElementById("cifrada").innerHTML = mensagemCifrada;
document.getElementById("decifrada").innerHTML = mensagemDecifrada;

